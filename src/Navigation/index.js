import * as React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import login from '../screens/login'
import imdb from '../screens/imdb'
import list from '../screens/list';
import trying from '../screens/trying';
import Statement from '../screens/Statement';
import None from '../screens/None';
import CRUD from '../screens/CRUD';
import SplashScreen from '../screens/SplashScreen';
import Redux from '../screens/Redux';
import listloop from '../screens/listloop';
import Ganjil from '../screens/Ganjil';
import Genap from '../screens/Genap';
import Prima from '../screens/Prima';
import Fibonacci from '../screens/FIbonacci';
import Post from '../screens/Post';
import Users from '../screens/Users';
import Anastasia from '../screens/Anastasia';
import Firebase from '../screens/Firebase';
import SignUp from '../screens/SignUp';
import FirestoreIMP from '../screens/FirestoreIMP';
import login2 from '../screens/login2';
import Assignment3 from'../screens/Assignment3';
import Inbox from '../screens/Inbox';
import inboxScreen from '../screens/inboxScreen';
import inboxRedux from '../screens/inboxRedux';

const Stack = createNativeStackNavigator();

function index() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName='SplashScreen'>
        <Stack.Screen name="SplashScreen" component={SplashScreen} options={{ headerShown: false }} />
        <Stack.Screen name="login2" component={login2} options={{ headerShown: false }} />
        <Stack.Screen name="Redux" component={Redux} options={{ headerShown: false }} />
        <Stack.Screen name="listloop" component={listloop} options={{ headerShown: false }} />
        <Stack.Screen name="Ganjil" component={Ganjil} options={{ headerShown: false }} />
        <Stack.Screen name="Genap" component={Genap} options={{ headerShown: false }} />
        <Stack.Screen name="Prima" component={Prima} options={{ headerShown: false }} />
        <Stack.Screen name="Fibonacci" component={Fibonacci} options={{ headerShown: false }} />
        <Stack.Screen name="Post" component={Post} options={{ headerShown: false }} />
        <Stack.Screen name="Users" component={Users} options={{ headerShown: false }} />
        <Stack.Screen name="Anastasia" component={Anastasia} options={{ headerShown: false }} />
        <Stack.Screen name="Firebase" component={Firebase} options={{ headerShown: false }} />
        <Stack.Screen name="FirestoreIMP" component={FirestoreIMP} options={{ headerShown: false }} />
        <Stack.Screen name="SignUp" component={SignUp} options={{ headerShown: false }} />
        <Stack.Screen name="login" component={login} options={{ headerShown: false }} />
        <Stack.Screen name="imdb" component={imdb} options={{ headerShown: false }} />
        <Stack.Screen name="list" component={list} options={{ headerShown: false }} />
        <Stack.Screen name="trying" component={trying} options={{ headerShown: false }} />
        <Stack.Screen name="Statement" component={Statement} options={{ headerShown: false }} />
        <Stack.Screen name="None" component={None} options={{ headerShown: false }} />
        <Stack.Screen name="CRUD" component={CRUD} options={{ headerShown: false }} />
        <Stack.Screen name="Assignment3" component={Assignment3} options={{ headerShown: false }} />
        <Stack.Screen name="Inbox" component={Inbox} options={{ headerShown: false }} />
        <Stack.Screen name="inboxScreen" component={inboxScreen} options={{ headerShown: false }} />
        <Stack.Screen name="inboxRedux" component={inboxRedux} options={{ headerShown: false }} />

      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default index;