import React, { Component } from 'react';
import { Text, StyleSheet } from 'react-native';

export default class CText extends Component {
    render() {
        const { style } = this.props;
        return (
            <Text
                {...this.state} style={{ ...stylez.teks, ...style  }}>
                {this.props.children}
            </Text>
        );
    }
}

const stylez = StyleSheet.create({
    teks: {
        justifyContent: 'center',
        alignSelf: 'center',
        fontSize: 15,
        color: 'pink',
        margin: 10,
    }
})