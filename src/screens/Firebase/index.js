import React, { Component } from 'react';
import { Text, View } from 'react-native';
import firestore from '@react-native-firebase/firestore';

export default class Firebase extends Component {
    constructor() {
        super()
        this.state = {
            data: [],
        }
    }

    componentDidMount() {
        firestore()
            .collection('UserID')
            .doc('MOi2L68cKLAHwwwD09fJ')
            .onSnapshot(Response => {
                this.setState({ data: Response.data() })
            });
    }

    render() {
        const { data } = this.state;
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Text>Test Firebase</Text>
                <Text>{data && JSON.stringify(data)}</Text>
            </View>
        )
    }
}

