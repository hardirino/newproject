import React, { Component } from 'react';
import { Text, View } from 'react-native';

export default class index extends Component {
    constructor() {
        super()
        this.state = {
            data: 'Prima',
            array: [
                1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21
            ],
            prima: []
        }
    };

    apakahPrima = (angka) => {
        let pembagi = 0;
        for (let i = 1; i <= angka; i++) {
            if (angka % i == 0) {
                pembagi++
            }
        }
        if (pembagi == 2) {
            return pembagi;
        } else {
            return null;

        }
    }

    componentDidMount() {
        let contain = [];
        this.state.array.map(x => {
            if (this.apakahPrima(x)) {
                contain.push(x)
            }
        })
        this.setState({ prima: contain })
    }

    render() {
        const { data } = this.state;
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Text style={{fontWeight:'bold',fontSize:20}}>{data}</Text>
                <View style={{ alignItems: 'center', justifyContent: 'center' }} >
                    <Text style={{fontWeight:'bold',fontSize:20}}>{this.state.prima.toString()}</Text>
                </View>
            </View>
        )
    }

}