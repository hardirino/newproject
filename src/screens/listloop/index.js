import React,{Component} from 'react';
import {Text, View, StyleSheet, Button} from 'react-native';

export default class index extends Component {

    render(){
        return(
            <View style={{flex:1, alignItems:'center',justifyContent:'space-evenly',backgroundColor:'pink'}}>
                <Button title='Ganjil'
                onPress={()=>{this.props.navigation.navigate('Ganjil')}}
                />
                <Button title='Genap'
                onPress={()=>{this.props.navigation.navigate('Genap')}}
                />
                <Button title='Prima'
                onPress={()=>{this.props.navigation.navigate('Prima')}}
                />
                <Button title='Fibonacci'
                onPress={()=>{this.props.navigation.navigate('Fibonacci')}}
                />
                <Button title='Post'
                onPress={()=>{this.props.navigation.navigate('Post')}}
                />
                <Button title='Users'
                onPress={()=>{this.props.navigation.navigate('Users')}}
                />
                <Button title='Anastasia'
                onPress={()=>{this.props.navigation.navigate('Anastasia')}}
                />
                <Button title='Firebase'
                onPress={()=>{this.props.navigation.navigate('Firebase')}}
                />
                <Button title='Redux'
                onPress={()=>{this.props.navigation.navigate('Redux')}}
                />
                <Button title='SignUp'
                onPress={()=>{this.props.navigation.navigate('SignUp')}}
                />
                <Button title='Login Screen Axios'
                onPress={()=>{this.props.navigation.navigate('login')}}
                />
                <Button title='Login Screen Firebase'
                onPress={()=>{this.props.navigation.navigate('login2')}}
                />
                <Button title='Assignment 4'
                onPress={()=>{this.props.navigation.navigate('Assignment3')}}
                />
                <Button title='Assignment 3'
                onPress={()=>{this.props.navigation.navigate('inboxRedux')}}
                />
                <Button title='IMDB'
                onPress={()=>{this.props.navigation.navigate('imdb')}}
                />
            </View>
        )
    }
}