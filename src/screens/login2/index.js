import React, { Component } from 'react';
import {
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Alert
}
  from 'react-native';
import auth from '@react-native-firebase/auth';

export default class login2 extends Component {
  constructor() {
    super()
    this.state = {
      secure: true,
      data: {},
      email: '',
      password: '',
    }
  }

  componentDidMount() {
  }

  _functionAuth = () => {
    const { email, password } = this.state;
    auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        this.props.navigation.navigate('Assignment3')
      })
      .catch(error => {
        if (error.code === 'auth/email-already-in-use') {
          console.log('That email address is already in use!');
        }
        if (error.code === 'auth/invalid-email') {
          console.log('That email address is invalid!');
        }
        Alert.alert('Email or Password is Wrong!');
      })
  }


  render() {
    const { email, password } = this.state;
    return (
      <View style={{ flex: 1 }}>
        <View style={model.container}>
          <Image source={require('/Users/hardi/NewProject/src/media/Blekpink.jpg')}
            style={{
              marginTop: 10,
              marginBottom: 10,
              height: 200,
              alignSelf: 'center',
            }}
            resizeMode={'center'} />
          <Text style={model.pinks}>
            Black Pink!
          </Text>
          <ImageBackground source={require('/Users/hardi/NewProject/src/media/Concert.jpg')}
            style={{ height: 400, width: 500, alignSelf: 'center' }}
            resizeMode={'cover'}>
            <Text style={{
              color: 'pink',
              alignSelf: 'center',
              fontWeight: 'bold',
              marginTop: 20,
              marginBottom: 20,
              fontSize: 15,
            }}>
              Your BlackPink E-Concert
            </Text>
            <TextInput onChangeText={v => this.setState({ email: v })}
              style={model.Isidalam}
              placeholder="Username"
              placeholderTextColor="black">
            </TextInput>
            <TextInput onChangeText={v => this.setState({ password: v })}
              style={model.Isidalam}
              placeholder="Password"
              secureTextEntry={true}
              placeholderTextColor="black"

            >
            </TextInput>
            <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
              <TouchableOpacity style={model.Dipencet}
                onPress={() => { this._functionAuth() }}>
                <Text style={{ color: "white", fontWeight: "bold" }}>Log In</Text>
              </TouchableOpacity>
              <TouchableOpacity style={model.Dipencet}
                onPress={() => { this.props.navigation.navigate('SignUp') }}>
                <Text style={{ color: "white", fontWeight: "bold" }}>Sign Up</Text>
              </TouchableOpacity>
            </View>
            <View style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'center'
            }}>
              <TouchableOpacity style={{ marginTop: 10, marginRight: 10, }}
                onPress={() =>
                  Alert.alert('Beta Version, not Ready yet.')}>
                <Image source={require('/Users/hardi/NewProject/src/media/pesbuk.jpg')}
                  style={{
                    width: 60,
                    height: 60,
                    marginLeft: 100,
                    backgroundColor: 'pink',
                    borderRadius: 15
                  }} />
              </TouchableOpacity>
              <TouchableOpacity style={{ marginTop: 10, marginLeft: 10, }}
                onPress={() =>
                  Alert.alert('Beta Version, not Ready yet.')}>
                <Image source={require('/Users/hardi/NewProject/src/media/gugel.jpg')}
                  style={{
                    width: 60,
                    height: 60,
                    marginRight: 100,
                    backgroundColor: 'pink',
                    borderRadius: 15
                  }} />
              </TouchableOpacity>
            </View>
          </ImageBackground>
        </View>
      </View >
    )
  }
}

const model = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'pink',
  },
  pinks: {
    color: 'pink',
    fontStyle: 'normal',
    fontSize: 60,
    justifyContent: 'center',
    alignSelf: 'center',
    backgroundColor: 'black',
    fontWeight: 'bold',
    height: 100,
    width: 500,
    alignItems: 'center',
    textAlign: 'center',
    paddingTop: 7,
  },
  Isidalam: {
    alignSelf: 'center',
    backgroundColor: 'pink',
    width: 210,
    height: 50,
    alignItems: 'center',
    marginTop: 5,
    textAlign: 'center',
    marginBottom: 4,
    borderRadius: 20,
    fontWeight: 'bold',
    borderWidth: 4,
    borderColor: 'black',
  },
  Dipencet: {
    marginTop: 10,
    backgroundColor: 'black',
    justifyContent: 'center',
    width: 100,
    height: 60,
    alignSelf: 'center',
    alignItems: 'center',
    borderRadius: 100,
    borderColor: 'pink',
    borderWidth: 3,
    elevation: 30,
  },

})

// import React, { Component } from 'react'
// import { Image, StyleSheet, Text, View } from 'react-native'

// export default class App extends Component{
//   render () {
//     return (
//       <View style={{flex:1}}>
//         <View style={Gambar.Warna1}>
//         </View>
//       <View style={Gambar.Warna2}>
//         <View style={Gambar.Isinya2}/>
//         <View style={Gambar.Isinya3}/>
//         </View>
//       <View style={Gambar.Warna3}>
//         <View style={Gambar.Isinya4}/>
//         <View style={Gambar.Isinya5}/>
//         <View style={Gambar.Isinya6}/>
//         </View>
//       </View>
//     );
//   }
// }
// const Gambar = StyleSheet.create({
//   Warna1: {
//     flex: 2,
//     backgroundColor: 'yellow',
//   },
//   Warna2: {
//     flex: 2,
//     backgroundColor: 'pink',
//   },
//   Isinya2: {
//     flex: 1,
//     backgroundColor: 'brown',
//     alignSelf: 'flex-end',
//     width: 200,
//   },
//   Isinya3: {
//     flex: 1,
//     backgroundColor: 'yellow',
//     alignSelf: 'flex-end',
//     width: 200,
//   },
//   Warna3: {
//     flex: 2,
//     flexDirection: 'row',
//     backgroundColor: 'navy',
//   },
//   Isinya4: {
//     flex: 0.65,
//     backgroundColor: 'gray',
//     flexDirection: 'row',
//     marginRight: 4,
//     marginLeft: 4,
//   },
//   Isinya5: {
//     flex: 0.65,
//     backgroundColor: 'green',
//     marginRight: 4,
//     marginLeft: 4,
//   },
//   Isinya6: {
//     flex: 0.65,
//     backgroundColor: 'red',
//     marginLeft: 4, 
//     marginRight: 4,
//   },
// })

