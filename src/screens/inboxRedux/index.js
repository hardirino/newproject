import React, { Component } from 'react';
import { Text, View, Image, StyleSheet, TouchableOpacity, FlatList } from 'react-native';
import AsyncStorageLib from '@react-native-async-storage/async-storage';
import messaging from '@react-native-firebase/messaging';
import Icon from 'react-native-vector-icons/AntDesign'
import CHeader from '../../components/atoms/CHeader';
import CLayout from '../../components/atoms/CLayout';
import { connect } from 'react-redux';

 class Index extends Component {
    constructor() {
        super()
        this.state = {
            data: [],
        }
    }

    componentDidMount() {
        // this._getData()
        messaging()
            .getToken()
            .then(fcmToken => {
                // console.log(fcmToken)
            });
    }

    _delete = () => {
        this.props.delete();
    };


    render() {
        const { inbox } = this.props;
        // const data = inbox;
        console.log(inbox)
        return (
            <CLayout>
                <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                    <Icon name='left' style={{ color: 'pink', fontSize: 30, marginTop: 10 }}
                        onPress={() => { this.props.navigation.navigate('inboxRedux') }} />
                    <CHeader>
                        Inbox
                    </CHeader>
                    <TouchableOpacity style={stylez.deled} onPress={() => { this._delete() }}>
                        <Text style={stylez.tex}>
                            Delete All
                        </Text>
                    </TouchableOpacity>
                </View>
                <FlatList
                    data={inbox}
                    renderItem={({ item }) => (
                        <TouchableOpacity
                            onPress={() => {
                                console.log(item)
                                this.props.navigation.navigate('inboxScreen', {
                                    message: {
                                        title: item.title,
                                        body: item.body,
                                        img: item.img
                                    }
                                })
                            }}>
                            <View style={{ borderRadius: 20, borderWidth: 5, margin: 2.5, backgroundColor:'pink' }}>
                                <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
                                    <Text
                                        style={{
                                            color: 'black',
                                            fontSize: 18,
                                            fontWeight: 'bold',
                                            margin: 5,
                                            paddingTop: 3,
                                            alignSelf: 'center',
                                        }}>
                                        {item.title}
                                    </Text>
                                </View>
                                <View style={{ borderRadius: 5, borderColor: 'black', borderWidth: 5, margin: 5 }}>
                                    <Text
                                        style={{
                                            color: 'black',
                                            fontSize: 18,
                                            alignSelf: 'center',
                                            paddingLeft: 5,
                                            height: 25,
                                        }}>
                                        {item.body}
                                    </Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    )}
                />
            </CLayout>
        )
    }
}

const mapStateToProps = (state) => {
    const {inbox} = state;
    return{inbox}
}
const mapDispatchToProps = dispatch => {
    return {
        delete: data => {
            dispatch({
                type: 'DELETE-MESSAGES',
                payload: data,
            })
        },
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Index)

const stylez = StyleSheet.create({
    deled: {
        backgroundColor: '#B33030',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        marginLeft: 95,
        marginTop: 10,
        width: 80,
    },
    tex: {
        fontWeight: 'bold',
        fontSize: 15,
    }
})