import React, { Component } from 'react';
import { Text, View, Button, TextInput, TouchableOpacity } from 'react-native';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

export default class FirestoreIMP extends Component {
    constructor() {
        super()
        this.state = {
            data: []
        }
    }

    componentDidMount() {
        firestore()
            .collection('BakpaoGoreng')
            .doc('Bakpao-GorenK')
            .onSnapshot(Response => {
                this.setState({ data: Response.data() })
            });
    }

    _delete = (identifier) => {
        const { data } = this.state;
        const head = data.filter(data => { return data.Stroberi != identifier })
        this.setState({
            data: head
        })
    }

    render() {
        const { data } = this.state;
        return (
            <View style={{ flex: 1, backgroundColor: 'pink' }}>
                <View style={{ alignSelf: 'center', justifyContent: 'center' }}>
                    <Text style={{ fontSize: 30, }}>
                        Bakpaonya Kakak~
                    </Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: 10 }}>
                    <View>
                    <Text>
                        Stroberi
                    </Text>
                    <Text>
                        Coklat
                    </Text>
                    <Text>
                        Daging
                    </Text>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: 5 }}>
                    <Text>{data.Stroberi}</Text>
                    <Text>{data.Coklat}</Text>
                    <Text>{data.Daging}</Text>
                    <Button
                        title='delete'
                        onpress={() => { this._delete(v.Stroberi) }} />
                </View>
            </View>
        )
    }
}