import React, { Component } from 'react';
import { Text, View, Image, StyleSheet, TouchableOpacity, FlatList } from 'react-native';
import AsyncStorageLib from '@react-native-async-storage/async-storage';
import messaging from '@react-native-firebase/messaging';
import Icon from 'react-native-vector-icons/AntDesign'
import CHeader from '../../components/atoms/CHeader';
import CLayout from '../../components/atoms/CLayout';

export default class Index extends Component {
    constructor() {
        super()
        this.state = {
            data: [],
        }
    }

    componentDidMount() {
        this._getData()
        // messaging()
        //     .getToken()
        //     .then(fcmToken => {
        //         console.log(fcmToken)
        //     });
    }

    _getData = async () => {
        try {
            const jsonValue = await AsyncStorageLib.getItem('newInboxData');
            return (
                jsonValue != null & this.setState({ data: JSON.parse(jsonValue) })
            )
        } catch (e) {
            console.log(e)
        }
    }

    _removeItemValue = async () => {
        try {
            await AsyncStorageLib.removeItem('newInboxData');
            this.setState({ data: [] })
        }
        catch (e) {
            console.log(e)
        }
    }

    render() {
        const { data } = this.state;
        return (
            <CLayout>
                <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                    <Icon name='left' style={{ color: 'pink', fontSize: 30, marginTop: 10 }}
                        onPress={() => { this.props.navigation.navigate('Assignment3') }} />
                    <CHeader>
                        Inbox
                    </CHeader>
                    <TouchableOpacity style={stylez.deled} onPress={() => { this._removeItemValue() }}>
                        <Text style={stylez.tex}>
                            Delete All
                        </Text>
                    </TouchableOpacity>
                </View>
                <FlatList
                    data={data}
                    renderItem={({ item, i }) => (
                        <TouchableOpacity
                            onPress={() => {
                                console.log(item)
                                this.props.navigation.navigate('inboxScreen', {
                                    message: {
                                        title: item.title,
                                        body: item.body,
                                        img: item.img
                                    }
                                })
                            }}>
                            <View style={{ borderRadius: 20, borderWidth: 5, margin: 2.5, backgroundColor:'pink' }}>
                                <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
                                    <Text
                                        style={{
                                            color: 'black',
                                            fontSize: 18,
                                            fontWeight: 'bold',
                                            margin: 5,
                                            paddingTop: 3,
                                            alignSelf: 'center',
                                        }}>
                                        {item.title}
                                    </Text>
                                </View>
                                <View style={{ borderRadius: 5, borderColor: 'black', borderWidth: 5, margin: 5 }}>
                                    <Text
                                        style={{
                                            color: 'black',
                                            fontSize: 18,
                                            alignSelf: 'center',
                                            paddingLeft: 5,
                                            height: 25,
                                        }}>
                                        {item.body}
                                    </Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    )}
                />
            </CLayout>
        )
    }
}

const stylez = StyleSheet.create({
    deled: {
        backgroundColor: '#B33030',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        marginLeft: 95,
        marginTop: 10,
        width: 80,
    },
    tex: {
        fontWeight: 'bold',
        fontSize: 15,
    }
})