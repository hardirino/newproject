import React, { Component } from 'react';
import {Text, View} from 'react-native';
import axios from 'axios';

export default class Post extends Component {
  constructor() {
    super()
    this.state = {
      data: [{}],
    }
  }

  componentDidMount() {
    axios.get('https://jsonplaceholder.typicode.com/comments?postId=1')
    .then(Response => {
      this.setState({ data: Response.data })
    })
  }
  

  render() {
    const { data } = this.state;
    return (
    <View>
      {data && data.map((v, i) => {
        return (
          <View key={i}>
            <Text>{v.id}</Text>
            <Text>{v.name}</Text>
            <Text>{v.body}</Text>
          </View>
        )
      })}
    </View>
    )}
}

