import React,{Component} from 'react';
import {Text, View, StyleSheet} from 'react-native';

export default class index extends Component {
    constructor(){
        super()
        this.state = {
            data: 'Genap',
            array: [
                1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21
            ]
        }
    };

    render(){
        const {data,array} = this.state;
        return(
            <View style={{flex:1,alignItems:'center', justifyContent:'center'}}>
                <Text style={{fontWeight:'bold'}}>{data}</Text>
                {array.map((v, i)=>
                {
                    return (
                        <View style={{alignItems:'center',justifyContent:'center'}} key={i}>
                            {v % 2 == 0 && <Text>{v}</Text>}
                        </View>
                    )
                })}
            </View>
        )
    }

}