import React, { Component } from 'react';
import { Text, View } from 'react-native';
import axios from 'axios';

export default class Anastasia extends Component {
    constructor() {
        super()
        this.state = {
            title: 'Website',
            data: []
        }
    }

   componentDidMount(){
       axios.get('https://jsonplaceholder.typicode.com/users')
       .then(Response=>{this.setState({data:Response.data})})
   }
    render() {
        const { title,data } = this.state;
        return (
            <View style={{flex:1, alignContent: 'center', justifyContent:'center'}}>
                {data.map((v,i)=>{
                    if( v.website == 'anastasia.net')
                    return (
                        <View>
                            <Text>{title}</Text>
                            <Text>{v.website}</Text>
                        </View>
                    )
                })}
            </View>
        )

    }
}
