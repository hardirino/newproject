import React, { Component } from 'react';
import { Text, Image, View, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign'
import CLayout from '../../components/atoms/CLayout';
import CTextContainer from '../../components/atoms/CTextContainer';
import CViewContainer from '../../components/atoms/CViewContainer';
import CHeader from '../../components/atoms/CHeader';

export default class index extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    componentDidMount() { }

    render() {
        const { message } = this.props.route.params;
        return (
            <CLayout>
                <View style={{ flexDirection: 'row', marginBottom: 10}}>
                    <Icon name='left' style={{ color: 'pink', fontSize: 30, marginTop: 10 }}
                        onPress={() => { this.props.navigation.navigate('inboxRedux') }} />
                    <View>
                        <CHeader style={{marginLeft:140}}>
                            Details..
                        </CHeader>
                    </View>
                </View>
                <CViewContainer>
                    <View>
                        <CTextContainer style={{ fontSize:17,fontWeight: 'bold' }}>
                            {message.title}
                        </CTextContainer>
                    </View>
                    <View>
                        <CTextContainer>
                            {message.body}
                        </CTextContainer>
                    </View>
                    <View style={{alignSelf:'center'}}>
                            <Image style={{width:150, height:150}} source={{uri:message.img}}/>
                    </View>
                </CViewContainer>
            </CLayout>
        )
    }
}