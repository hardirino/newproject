import React, { Component } from 'react';
import { Text, View, StyleSheet, Button, TextInput } from 'react-native';
import { connect } from 'react-redux';


export class index extends Component {
    constructor() {
        super()
        this.state = {
            data: [],
            name: '',
            address: '',
            id: '',
            onSelect: false,
            onEdit: false,
            checked: true,
        };
    }

    componentDidMount() { };


    //Flow dari Redux> Reducer.js>MapState & Map Dispatch> masuk ke Function yaitu _addData > di Call
    _addData = () => {
        const { name, address, id } = this.state;
        const data = { name, address, id }
        this.props.add(data);
        this.setState({
            id: '',
            name: '',
            address: '',
        })
    };

    _delete = id => {
        this.props.delete(id);
    };

    _onEdit = value => {
        this.setState({
            onEdit: !this.state.onEdit,
            id: value.id,
            name: value.name,
            address: value.address,
        });
    };

    _submitUpdate = () => {
        const { id, name, address } = this.state;
        const { students } = this.props;
        const data = { id, name, address };
        console.log(data)
        const updateStudents = students.map(value => {
            if (value.id === data.id) {
                (value.id = data.id),
                    (value.name = data.name),
                    (value.address = data.address);
                return value;
            } else {
                return value;
            }
        });
        this.props.update(updateStudents)
        this.setState({
            onEdit: false,
            name: '',
            address: '',
            id: ''
        });
    };

    _cancel = () => {
        this.setState({
            name: '',
            id: '',
            address: '',
            onEdit: false,
        });
    };


    render() {
        const { data, name, address, id, onEdit, checked } = this.state;
        const { students } = this.props;
        return (
            <View style={{flex:1}}>
                <View style={stylez.container}>
                    <View style={stylez.container1}>
                        <Text style={{ fontWeight: 'bold', fontSize: 20, }}>Id</Text>
                        <Text style={{ fontWeight: 'bold', fontSize: 20, }}>Name</Text>
                        <Text style={{ fontWeight: 'bold', fontSize: 20, }}>Address</Text>
                        <Text style={{ fontWeight: 'bold', fontSize: 20, }}>Action</Text>
                    </View>
                    {students && students.map((v, i) => {
                        return (
                            <View key={i} style={{ paddingTop: 10, flex: 1, flexDirection: 'row', justifyContent: 'space-around' }}>
                                <Text style={{ fontWeight: 'bold' }}>{v.id}</Text>
                                <Text style={{ fontWeight: 'bold' }}>{v.name}</Text>
                                <Text style={{ fontWeight: 'bold' }}>{v.address}</Text>
                                <View style={{}}>
                                    <Button
                                        title='Del'
                                        onPress={() => { this._delete(v.id) }} />
                                </View>
                                <View>
                                    <Button
                                        title='Edit'
                                        onPress={() => { this._onEdit(v) }} />
                                </View>
                            </View>
                        )
                    })}
                    <View>
                        <View style={stylez.textInput}>
                            <TextInput value={id} onChangeText={input => {
                                this.setState({ id: input });
                            }}
                                placeholder='Id Number'
                            />
                            <TextInput value={name} onChangeText={input => {
                                this.setState({ name: input });
                            }}
                                placeholder='Name' />
                            <TextInput value={address} onChangeText={input => {
                                this.setState({ address: input });
                            }}
                                placeholder='Address'
                            />

                        </View>
                        <Button title="Update"
                            onPress={() => { this._submitUpdate() }}
                        />
                        <Button title="Add Data"
                            onPress={() => { this._addData() }} />
                        <Button
                            title="cancel"
                            style={{ backgroundColor: 'red' }}
                            onPress={this._cancel}
                        />

                    </View>
                    <View>
                        <Button title='Looping Lists'
                            onPress={() => { this.props.navigation.navigate('listloop') }} />
                    </View>
                </View>
            </View>
        )
    }
}

const mapStateToProps = state => {
    return {
        students: state.students,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        add: data => {
            dispatch({
                type: 'ADD-STUDENT',
                payload: data,
            })
        },
        delete: data => {
            dispatch({
                type: 'DELETE-STUDENT',
                payload: data,
            })
        },
        update: data => {
            dispatch({
                type: 'UPDATE-STUDENT',
                payload: data,
            })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(index)

const stylez = StyleSheet.create({
    container: {
        padding: 10,
        borderRadius: 10,
        borderWidth: 4,
        margin: 5,
        flex: 1,
    },
    container1: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingRight: 50,
        paddingLeft: 10
    },
    textInput: {
        alignItems: 'center',
        backgroundColor: 'white',
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderRadius: 10,
        padding: 20,
    },
})

