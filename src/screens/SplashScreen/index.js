import React, { Component } from 'react';
import {
    Image,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    Text,
    View,
    ImageBackground,
    Alert
}
    from 'react-native';

export default class SplashScreen extends Component {

    componentDidMount(){
        const {navigation} = this.props;
        setTimeout(() => {
            navigation.replace('Redux')
        }, 3000);
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <Image 
                source={require('../../media/Splash.png')} 
                style={{maxWidth: 430, maxHeight: 700 }} 
                resizeMode='cover' />
            </View>
        )
    }
}