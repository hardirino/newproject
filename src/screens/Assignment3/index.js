import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';


export default class index extends Component {

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'pink' }}>
                <View style={{ paddingRight: 10, marginTop: 10, marginLeft: 10, flexDirection: 'row' }}>
                    <Icon
                        name='mail'
                        color='black'
                        size={50}
                        style={{ marginLeft: 330, }}
                        onPress={() => { this.props.navigation.navigate('Inbox') }} />
                </View>
            </View>
        )
    }
}