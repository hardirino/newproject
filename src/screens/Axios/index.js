import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  FlatList,
  Image,
  ActivityIndicator,
  TouchableOpacity,
  Alert,
  ScrollView,
  ImageBackground,
  TextInput,
} from 'react-native';
import axios from 'axios';

export default class Axios extends Component {
  constructor() {
    super()
    this.state = {
      data: [],
    }
  }
  componentDidMount() {
    axios.get('https://jsonplaceholder.typicode.com/users')
      .then(Response => { this.setState({ data: Response.data }) })
  }
  render() {
    const { data } = this.state;
    return (
    <View>
      {data && data.map((v, i) => {
        return (
          <View key={i}>
            <Text>{v.name}</Text>
            <Text>{v.email}</Text>
            <Text style={{ backgroundColor: 'pink', color: 'blue' }}>{v.address['i']}</Text>
          </View>
        )
      })}
    </View>
    )}
}

