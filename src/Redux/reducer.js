const initialState = {
    title: 'Redux',
    description: 'Learning Redux State Management',
    students: [
        { id: '1', name: 'Novendry', address: 'Indonesia' },
        { id: '2', name: 'Hardi', address: 'America' },
        { id: '3', name: 'Maspur', address: 'United Kingdom' },
        { id: '4', name: 'MasYog', address: 'Russia' },
        { id: '5', name: 'MasFarid', address: 'German' },
        { id: '6', name: 'MasAhmad', address: 'Ireland' },
        { id: '7', name: 'MasAli', address: 'Saudi Arabia' },
        { id: '8', name: 'MasSubhan', address: 'UAE' }
    ],
    inbox: []
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD-STUDENT':
            return {
                ...state,
                students: [...state.students, action.payload]
            }
        case 'DELETE-STUDENT':
            return {
                ...state,
                students: state.students.filter(value => {
                    return value.id != action.payload
                })
            }
        case 'DELETE-MESSAGES':
            return {
                ...state,
                inbox: []
            }
        case 'UPDATE-STUDENT':
            return {
                ...state,
                students: action.payload
            }
        case 'INBOX':
            return {
                ...state,
                inbox: [action.payload, ...state.inbox].slice(0, 10)
            }
        default:
            return state

    }
}

export default reducer