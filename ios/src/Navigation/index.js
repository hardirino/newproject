import * as React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import login from '../screens/login'
import imdb from '../screens/imdb'
import list from '../screens/list';
import trying from '../screens/trying';
import Statement from '../screens/Statement';
import Assignment2 from '../screens/Assignment2';
import CRUD from '../screens/CRUD';
import SplashScreen from '../screens/SplashScreen';

const Stack = createNativeStackNavigator();

function index() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="SplashScreen" component={SplashScreen} options={{ headerShown: false }} />
        <Stack.Screen name="login" component={login} options={{ headerShown: false }} />
        <Stack.Screen name="imdb" component={imdb} options={{ headerShown: false }} />
        <Stack.Screen name="list" component={list} options={{ headerShown: false }} />
        <Stack.Screen name="trying" component={trying} options={{ headerShown: false }} />
        <Stack.Screen name="Statement" component={Statement} options={{ headerShown: false }} />
        <Stack.Screen name="Assignment2" component={Assignment2} options={{ headerShown: false }} />
        <Stack.Screen name="CRUD" component={CRUD} options={{ headerShown: false }} />

      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default index;