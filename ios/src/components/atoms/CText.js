import React, { Component } from 'react';
import { Text, StyleSheet, TouchableOpacity } from 'react-native';

export default class CText extends Component {
    render() {
        const { style, title } = this.props;
        return (
            <Text
                {...this.state} style={{ fontWeight: 'bold', fontSize: 40, ...style }}>
                {this.props.children}
            </Text>
        );
    }
}

