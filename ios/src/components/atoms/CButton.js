import React, { Component } from 'react';
import { Text, StyleSheet, TouchableOpacity } from 'react-native';

export default class CButton extends Component {
    render() {
        const { WarnaContainer, WarnaTeks, title } = this.props;
        return (
            <TouchableOpacity {...this.props} style={{ ...styles.tombol, ...WarnaContainer }}>
                <Text 
                    style={{ fontWeight: 'bold', fontSize: 40, ...WarnaTeks }}>
                    {title.toLowerCase()}</Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    tombol: {
        backgroundColor: 'pink',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        marginBottom: 20,

    }
})