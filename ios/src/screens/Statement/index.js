import React, { Component } from 'react';
import { Text, StyleSheet, View, TextInput, Button, TouchableOpacity } from 'react-native';
import CButton from '../../components/atoms/CButton';

export default class Statement extends Component {
    constructor() {
        super();
        this.state = {
            name: '',
            address: '',
            data: {
                name: 'Udin',
                address: 'Yogya',
            },
            privacy: [
                { no: 1, Kota: 'Jakarta', Jalan: 'Hasanudin' },
                { no: 2, Kota: 'Yogya', Jalan: 'Kaliurang' },
                { no: 3, Kota: 'Samarinda', Jalan: 'Sudirman' },
                { no: 4, Kota: 'Pekanbaru', Jalan: 'Arifin Ahmad' },
                { no: 5, Kota: 'Bandung', Jalan: 'Soekarno' },
            ],
        };

    }

    componentDidMount() { }

    _submit() {
        this.setState({
            data: {
                name: this.state.name,
                address: this.state.address,
            },
        });
    }

    _trythis() {
        const { privacy, no, Kota, Jalan } = this.state;
        this.setState({
            privacy: [...privacy, { no, Kota, Jalan }
            ], no: '', Kota: '', Jalan: '',
        })
    }

    render() {
        const { data, privacy, no, Kota, Jalan } = this.state;
        return (
            <View style={{ flex: 1 }}>
                <Text style={{ textAlign: 'center', marginVertical: 10 }}>
                    Statement
                </Text>
                <View style={styles.paper}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-around', }}>
                        <Text style={{ fontWeight: 'bold' }}>
                            Nama
                        </Text>
                        <Text style={{ fontWeight: 'bold' }}>
                            Alamat
                        </Text>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                        <Text style={{ fontSize: 40, fontWeight: 'bold' }}>
                            {data.name}
                        </Text>
                        <Text style={{ fontSize: 40, fontWeight: 'bold' }}>
                            {data.address}
                        </Text>
                    </View>
                    <View style={styles.textInput}>
                        <TextInput onChangeText={input => {
                            this.setState({ name: input });
                        }}
                            placeholder='enter name' />
                        <TextInput onChangeText={input => {
                            this.setState({ address: input });

                        }}
                            placeholder='enter address'
                        />
                        <Button title="Change State"
                            onPress={() => { this._submit() }} />
                    </View>
                </View>
                <View style={styles.paper}>
                    <View style={styles.private}>
                        <Text style={{ fontSize: 20, fontWeight: 'bold', maxWidth: 30, marginHorizontal: 5 }}>No</Text>
                        <Text style={{ fontSize: 20, fontWeight: 'bold', maxWidth: 100, marginHorizontal: 5 }}>Kota</Text>
                        <Text style={{ fontSize: 20, fontWeight: 'bold', maxWidth: 100, marginHorizontal: 5, }}>Jalan</Text>
                    </View>
                    {privacy.map((v, i) => {
                        return (
                            <View key={i} style={styles.private}>
                                <Text>{v.no}</Text>
                                <Text>{v.Kota}</Text>
                                <Text>{v.Jalan}</Text>
                            </View>
                        )
                    }
                    )}
                    <View>
                        <View style={styles.textInput}>
                            <TextInput value={no} onChangeText={input => {
                                this.setState({ no: input });
                            }}
                                placeholder='House Number'
                            />
                            <TextInput value={Kota} onChangeText={input => {
                                this.setState({ Kota: input });
                            }}
                                placeholder='City name' />
                            <TextInput value={Jalan} onChangeText={input => {
                                this.setState({ Jalan: input });
                            }}
                                placeholder='Address name'
                            />

                        </View>
                        <Button title="Change State"
                            onPress={() => { this._trythis() }} />
                    </View>
                    <CButton
                        title='Tombol'
                        WarnaTeks={{color: 'blue', fontSize: 20,}}
                        WarnaContainer={{ backgroundColor: 'pink', elevation:5 }} />
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    paper: {
        padding: 10,
        borderRadius: 10,
        borderWidth: 4,
        margin: 5,
    },
    textInput: {
        alignItems: 'center',
        marginTop: 10,
        backgroundColor: 'pink',
        flexDirection: 'row',
        justifyContent: 'center',
    },
    private: {
        flexDirection: 'row',
        justifyContent: 'space-between',

    }
});