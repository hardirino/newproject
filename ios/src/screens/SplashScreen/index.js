import React, { Component } from 'react';
import {
    Image,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    Text,
    View,
    ImageBackground,
    Alert
}
    from 'react-native';

export default class SplashScreen extends Component {

    componentDidMount(){
        const {navigation} = this.props;
        setTimeout(() => {
            navigation.replace('login')
        }, 1500);
    }

    render() {
        return (
            <View>
                <Image 
                source={require('../../media/launch_screen.png')} 
                style={{ width: 800, height: 1200 }} 
                resizeMode='center' />
            </View>
        )
    }
}