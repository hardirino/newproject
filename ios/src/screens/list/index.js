import React, { Component } from 'react';
import {
    Image,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    Text,
    View,
    ImageBackground,
    Alert
}
    from 'react-native';
import CButton from '../../components/atoms/CButton';

export default class list extends Component {
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'black' }}>
                <CButton
                    onPress={() => { this.props.navigation.navigate('trying') }}
                    title='Look Here' />
                <View style={model.container}>
                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('imdb') }}>
                        <Text style={{
                            fontSize: 40,
                            fontWeight: 'bold',
                            color: 'pink',
                        }}
                        > Assignment 1
                        </Text>
                    </TouchableOpacity>
                </View>
                <View style={model.container}>
                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('Assignment2') }}>
                        <Text style={{
                            fontSize: 40,
                            fontWeight: 'bold',
                            color: 'pink'
                        }}
                        > Assignment 2
                        </Text>
                    </TouchableOpacity>
                </View>
                <View style={model.container}>
                <TouchableOpacity onPress={() => { this.props.navigation.navigate('CRUD') }}>
                        <Text style={{
                            fontSize: 40,
                            fontWeight: 'bold',
                            color: 'pink'
                        }}
                        > Assignment 3
                        </Text>
                    </TouchableOpacity>
                </View>
            </View >
        )
    }
}

const model = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        borderWidth: 10,
        borderRadius: 30,
        borderColor: 'pink',
        backgroundColor: 'black',
        alignSelf: 'center',
        justifyContent: 'center',

    },
}
)