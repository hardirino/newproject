import React, { Component } from 'react';
import Navigation from './src/Navigation';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { Persistor, Store } from './src/Redux/store';
import messaging from '@react-native-firebase/messaging';
import AsyncStorageLib from '@react-native-async-storage/async-storage';


messaging()
  .onMessage(async (remoteMessage) => {
    Store.dispatch({
      type: 'INBOX', payload:
      {
        title: remoteMessage.notification.title,
        body: remoteMessage.notification.body,
        img: remoteMessage.notification.android.imageUrl,
        data: remoteMessage.data,
      }
    })
    console.log('2', remoteMessage)
    setInboxData(remoteMessage)
  });

messaging().
  setBackgroundMessageHandler(async (remoteMessage) => {
    Store.dispatch({
      type: 'INBOX', payload:
      {
        title: remoteMessage.notification.title,
        body: remoteMessage.notification.body,
        img: remoteMessage.notification.android.imageUrl,
        data: remoteMessage.data,
      }
    })
    setInboxData(remoteMessage)
    console.log(remoteMessage)
  });

const setInboxData = async remoteMessage => {
  try {
    let getInboxData = await AsyncStorageLib.getItem('newInboxData');
    getInboxData = JSON.parse(getInboxData);
    if (!getInboxData) {
      AsyncStorageLib.setItem('newInboxData',
        JSON.stringify(
          [
            {
              title: remoteMessage.notification.title,
              body: remoteMessage.notification.body,
              img: remoteMessage.notification.android.imageUrl,
              data: remoteMessage.data,
            }
          ]
        )
      )
    } else {
      const arrayData = [
        ...[
          {
            title: remoteMessage.notification.title,
            body: remoteMessage.notification.body,
            img: remoteMessage.notification.android.imageUrl,
            data: remoteMessage.data,
          }
        ],
        ...getInboxData
      ]
      AsyncStorageLib.setItem('newInboxData',
        JSON.stringify(arrayData) //setitem : menyimpan data / input data
      )
    }
  } catch (error) {
    console.log(error);
  }
}

const App = () => {
  return (
    <Provider store={Store}>
      <PersistGate loading={null} persistor={Persistor}>
        <Navigation />
      </PersistGate>
    </Provider>
  )
}

export default App;